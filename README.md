# README #

This is a C++ implementation of a non-interactive zero-knowledge shuffle
argument presented in [https://eprint.iacr.org/2017/894]
(https://eprint.iacr.org/2017/894). Implementation is based on the
[libff library](https://github.com/scipr-lab/libff).
Feel free to experiment with the code, but note that **this implementation is
ment as a prototype and should not be directly used in real life applications**.

### Overview ###
Shuffling is a process of randomly permuting and blinding (rerandomizing)
a set of ciphertexts. Zero-knowledge shuffle argument allows a party to prove
that it did the shuffle procedure correctly,
without leaking any information about the permutation.
Non-interactive shuffle argument allows the prover to send out a single messages
that can be verified by anyone, even if the original prover
is not available anymore.

Main application for shuffle argument is mix network that takes in a set of
ciphertexts and lets servers shuffle the ciphertexts one after another to
destroy the link between ciphertext and its source.
To guarantee that servers do not inject any of their own ciphertexts,
each server can provide a zero-knowledge proof.
Mix networks are used for e-voting and many other applications that need
to provide ciphertext anonymity.

Many efficient non-interactive shuffle arguments are known that guarantee
security in the random oracle model (ROM).
This unfortunately gives only a heuristic security
(cases are known where ROM proof does not guarantee security in real life).
The shuffle argument mentioned above is the most efficient construction so far
that avoids ROM.
It uses ElGamal cryptosystem.


### How to set it up ###

On Linux Ubuntu 16.10 it works as follows:

1. Start by recursively fetching the dependencies:

	```bash
	git submodule update --init --recursive
	```

2. Install libff dependecies:

	```bash
    sudo apt-get install build-essential git libboost-all-dev cmake libgmp3-dev libssl-dev libprocps4-dev pkg-config
	```

3. Next, initialize the `build` directory:

	```bash
	mkdir build && cd build && cmake ..
	```

4. Lastly, compile the library:

	```bash
	make
	```

5. To run the application, use the following command from the `build` directory:

	```bash
	./src/program 1000
	```

If the build directory is initialized with `cmake -DMULTICORE=ON ..`,
then the most expensive computations are parallelized.
This however needs some modification of libff library
(Pairings use some static profiling code that does not allow parallelization).
To fix the issue, add code from the folder libff_changes to the libff
folder depends/libff/libff/algebra/curves/bn128/.
After that libff should be recompiled and reinstalled.
