/*! \file */
/*
 * verifier.h
 * Created on: 29.12.2016
 */
#ifndef VERIFIER_H_
#define VERIFIER_H_

#include <vector>
#include <future>
#include "omp.h" //to get max threads
#include <libff/algebra/curves/public_params.hpp>
#include <libff/algebra/scalar_multiplication/multiexp.hpp>
#include "crs.h"
#include "shuffle_util.h"
#include "prover.h"
#include <iostream>

using namespace std;
using namespace libff;

/*! Verifies a zero-knowledge proof of a shuffle. It can separately verify offline and online phases, but offline phase must be verified before the online
* phase can be verified.
* \brief Verifies zero-knowledge proof of a shuffle.
* \tparam ppT elliptic curve group
*/
template<typename ppT>
class Verifier {

public:

	/*! Constructor for the verifier.
	* \param crs common reference string generated by a trusted third party
	*/
	Verifier(CRS<ppT> crs, int k): crs {crs}, k {k} {

#ifdef PARALLEL
		chunks = omp_get_max_threads(); //otherwise == 1
#endif

	}


	/*! Verifies offline part of the proof. This includes permutation matrix argument and the same-message argument.
	* \param offline_proof offline part of the proof
	* \returns true if offline proof is correct and false otherwise
	*/
	bool verify_offline(Offline_proof<ppT> offline_proof) {
		enter_block("Offline verifier", true);
		off_proof = offline_proof;

        bool correct = true;

		//Add last commitment
		off_proof.a_coms.push_back(crs.g1_sum_Pi - vector_sum<G1<ppT>>(offline_proof.a_coms));
		off_proof.a_hat_coms.push_back(crs.g1_sum_Pi_hat - vector_sum<G1<ppT>>(offline_proof.a_hat_coms));
		off_proof.b_coms.push_back(crs.g2_sum_Pi - vector_sum<G2<ppT>>(offline_proof.b_coms));

		enter_block("Permutation proof verification", false);
		inhibit_profiling_info = true;
		correct = correct && check_permutation_proof();
		inhibit_profiling_info = false;
		leave_block("Permutation proof verification", false);

        if (correct){
			enter_block("Same-message proof verification", false);
			inhibit_profiling_info = true;
			correct = correct && check_same_message_proof();
			inhibit_profiling_info = false;
			leave_block("Same-message proof verification", false);

			batch_elem3 = generate_randomizer<ppT>(k);

			inhibit_profiling_info = true;
			precomputation = ppT::reduced_pairing(off_proof.t_com, batch_elem3 * G2<ppT>::one() + crs.g2_sk);
			inhibit_profiling_info = false;
        }
		leave_block("Offline verifier", true);
		return correct;
	}


	/*! Verifies online part of the proof (consistency argument).
	This could be potentially more efficient if it would use fixed base exponentiation.
	* \param online_proof online part of the proof
	* \returns Online part of the proof.
	*/
	bool verify_online(ElGamal_vector<ppT> ciphertexts, Online_proof<ppT> online_proof) {
		enter_block("Online verifier = consistency argument", true);
		inhibit_profiling_info = true;

		int amount_per_thread = crs.n / chunks;

		int begin = 0;
		int end = amount_per_thread;
		vector<future<GT<ppT>>> thread_comps;

		for (size_t i = 0;  i < chunks; i++) {
			if (i == chunks - 1) end = crs.n - 1;
			thread_comps.push_back(async(launch::async, &Verifier::online_proof_left_chunk, this, begin, end, online_proof.output));
			begin = end + 1;
			end += amount_per_thread;
		}

		GT<ppT> lhs = GT<ppT>::one();
		for (unsigned int i = 0; i < thread_comps.size(); i++) {
			lhs = lhs * thread_comps.at(i).get();
		}
		lhs = lhs * ppT::reduced_pairing(crs.g1_rho_hat, batch_elem3 * online_proof.consist.first + online_proof.consist.second);


		begin = 0;
		end = amount_per_thread;
		vector<future<GT<ppT>>> thread_comps2;

		for (size_t i = 0;  i < chunks; i++) {
			if (i == chunks - 1) end = crs.n - 1;
			thread_comps2.push_back(async(launch::async, &Verifier::online_proof_right_chunk, this, begin, end, ciphertexts));
			begin = end + 1;
			end += amount_per_thread;
		}

		GT<ppT> rhs = precomputation;
		for (unsigned int i = 0; i < thread_comps.size(); i++) {
			rhs = rhs * thread_comps2.at(i).get();
		}
		inhibit_profiling_info = false;
		leave_block("Online verifier = consistency argument", true);
		return lhs == rhs;
	}


	/*! Verifies both offline and online part of the proof.
	* \param proof shuffle proof
	* \returns true if proof is correct and false otherwise.
	*/
	bool verify(ElGamal_vector<ppT> ciphertexts, Proof<ppT> proof) {
		bool correct = true;
		enter_block("Verifier", true);
		correct = correct && verify_offline(proof.first);
		if(correct){
			correct = correct && verify_online(ciphertexts, proof.second);
		}
		leave_block("Verifier", true);
		return correct;
	}


private:
	//! Number of threads that can be used
	size_t chunks = 1;

	//! Common reference string
	CRS<ppT> crs;

	//! Security parameter in number of bits. Batching randomizers are taken to be k bits long.
	int k;

	//! Offline proof
	Offline_proof<ppT> off_proof;

	//! Batching element p_{3_1}
	Fr<ppT> batch_elem3;

	//! Element e(t, (p3  + sk) * g2)
	GT<ppT> precomputation;


	/*! Checks if permutation matrix proof is correct
	* \returns true if argument holds and false otherwise
	*/
	bool check_permutation_proof() {
		Fr_vector<ppT> batch_randomizers = generate_randomizers<ppT>(crs.n - 1, k);
		batch_randomizers.push_back(Fr<ppT>::random_element());

		int amount_per_thread = crs.n / chunks;

		int begin = 0;
		int end = amount_per_thread;
		vector<future<GT<ppT>>> thread_comps;

        Fr<ppT> alpha = Fr<ppT>::random_element();
        G1<ppT> g1_alpha_P0 = (alpha * G1<ppT>::one())  + crs.g1_P0;
        G2<ppT> g2_neg_alpha_P0 = (-alpha * G2<ppT>::one()) + crs.g2_P0;

        GT<ppT> gt_alpha2 = ppT::reduced_pairing(G1<ppT>::one(), G2<ppT>::one()) ^ (Fr<ppT>::one() - (alpha ^ 2));

		for (size_t i = 0;  i < chunks; i++) {
			if (i == chunks - 1) end = crs.n - 1;
			thread_comps.push_back(async(launch::async, &Verifier::permutation_proof_product, this, batch_randomizers,
				g1_alpha_P0, g2_neg_alpha_P0, begin, end));
			begin = end + 1;
			end += amount_per_thread;
		}

		GT<ppT> lhs = GT<ppT>::one();
		for (unsigned int i = 0; i < thread_comps.size(); i++) {
			lhs = lhs * thread_comps.at(i).get();
		}

		G1<ppT> prod_rhs = multi_exp<G1<ppT>, Fr<ppT>, multi_exp_method_BDLO12>(off_proof.uvs.begin(), off_proof.uvs.end(),
		                  batch_randomizers.begin(), batch_randomizers.end(), chunks);

		GT<ppT> rhs = ppT::reduced_pairing(prod_rhs, crs.g2_rho) * (gt_alpha2 ^ (vector_sum<Fr<ppT>>(batch_randomizers)));

		return lhs == rhs;
	}


	/*! Checks if same-message proof is correct
	* \returns true if argument holds and false otherwise
	*/
	bool check_same_message_proof() {
		Fr_vector<ppT> batch_randomizers = generate_randomizers<ppT>(crs.n - 1, k);
		batch_randomizers.push_back(Fr<ppT>::one());
		G1<ppT> prod_lhs = multi_exp<G1<ppT>, Fr<ppT>, multi_exp_method_BDLO12>(off_proof.same_msgs.begin(), off_proof.same_msgs.end(),
		                   batch_randomizers.begin(), batch_randomizers.end(), chunks);

		G1<ppT> prod_rhs1 = multi_exp<G1<ppT>, Fr<ppT>, multi_exp_method_BDLO12>(off_proof.a_coms.begin(), off_proof.a_coms.end(),
		                    batch_randomizers.begin(), batch_randomizers.end(), chunks);
		G1<ppT> prod_rhs2 = multi_exp<G1<ppT>, Fr<ppT>, multi_exp_method_BDLO12>(off_proof.a_hat_coms.begin(), off_proof.a_hat_coms.end(),
		                    batch_randomizers.begin(), batch_randomizers.end(), chunks);

		GT<ppT> lhs = ppT::reduced_pairing(prod_lhs, G2<ppT>::one());
		GT<ppT> rhs = ppT::reduced_pairing(prod_rhs1, crs.g2_beta) * ppT::reduced_pairing(prod_rhs2, crs.g2_beta_hat);

		return lhs == rhs;
	}


	/*! Computes part of left hand side of permutation matrix proof verification. Part is fixed by begin and end index.
	*   This is used to paralellize online computation.
	* \param batch_randomizers randomizers used for batching
	* \param begin begin index
	* \end end index
	* \returns pairing product of values between begin and end index
	*/
	GT<ppT> permutation_proof_product(Fr_vector<ppT> batch_randomizers, G1<ppT> g1_alpha_P0, G2<ppT> g2_neg_alpha_P0,
		int begin, int end) {

		vector<G1_precomp<ppT>> precomps1;
		for (int i = begin; i <= end; i++) precomps1.push_back(ppT::precompute_G1(batch_randomizers.at(i) * (off_proof.a_coms.at(i) + g1_alpha_P0)));

		vector<G2_precomp<ppT>> precomps2;
		for (int i = begin; i <= end; i++) precomps2.push_back(ppT::precompute_G2(off_proof.b_coms.at(i) + g2_neg_alpha_P0));

		Fqk<ppT> result = Fqk<ppT>::one();
		for (unsigned int i = 0; i < precomps1.size(); i++) result = result * ppT::miller_loop(precomps1.at(i), precomps2.at(i));

		return ppT::final_exponentiation(result);
	}


	/*! Computes part of the left hand side pairings of online verification. Part is fixed by begin and end index.
	*  This is used to paralellize online computation.
	* \param begin begin index
	* \end end index
	* \returns pairing product of values between begin and end index
	*/
	GT<ppT> online_proof_left_chunk(int begin, int end, ElGamal_vector<ppT> shuffled_ciphers) {

		vector<G1_precomp<ppT>> precomps1;
		vector<G2_precomp<ppT>> precomps2;
		for (int i = begin; i <= end; i++) {
			precomps1.push_back(ppT::precompute_G1(crs.g1_Pi_hats.at(i)));
			precomps2.push_back(ppT::precompute_G2((batch_elem3 * shuffled_ciphers.at(i).first) + shuffled_ciphers.at(i).second));
		}

		Fqk<ppT> result = Fqk<ppT>::one();
		for (unsigned int i = 0; i < precomps1.size(); i++) result = result * ppT::miller_loop(precomps1.at(i), precomps2.at(i));

		return ppT::final_exponentiation(result);

	}


	/*!Computes part of the right hand side pairings of online verification. Part is fixed by begin and end index.
	*  This is used to paralellize online computation.
	* \param begin begin index
	* \end end index
	* \returns pairing product of values between begin and end index
	*/
	GT<ppT> online_proof_right_chunk(int begin, int end, ElGamal_vector<ppT> ciphertexts) {

		vector<G1_precomp<ppT>> precomps1;
		vector<G2_precomp<ppT>> precomps2;
		for (int i = begin; i <= end; i++) {
			precomps1.push_back(ppT::precompute_G1(off_proof.a_hat_coms.at(i)));
			precomps2.push_back(ppT::precompute_G2((batch_elem3 * ciphertexts.at(i).first) + ciphertexts.at(i).second));
		}

		Fqk<ppT> result = Fqk<ppT>::one();

		for (unsigned int i = 0; i < precomps1.size(); i++) result = result * ppT::miller_loop(precomps1.at(i), precomps2.at(i));

		return ppT::final_exponentiation(result);
	}
};

#endif /* VERIFIER_H_ */
