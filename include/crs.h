/*! \file */
/*
 * crs.h
 * Created on: 09.12.2016
 */
#ifndef CRS_H_
#define CRS_H_

#include <vector>
#include "gmp.h"
#include <libff/algebra/fields/bigint.hpp>
#include <libff/algebra/curves/public_params.hpp>
#include <libff/algebra/scalar_multiplication/multiexp.hpp>
#include <shuffle_util.h>


using namespace std;
using namespace libff;


/*! Common reference string (CRS) for the prover and the verifier.
* It contains elements of groups G1, G2. Groups G1 and G2 are additive (and GT is multiplicative).
* \brief Common reference string
* \tparam ppt elliptic curve group
*/
template<typename ppT>
struct CRS {

  //!Number of ciphertexts
  int n;


  //-----Group G1 elements of CRS-----

  //!Elements [P_i(chi)]_1 for i = 1, ..., n
  vector<G1<ppT>> g1_Pis;

  //!Element [rho]_1
  G1<ppT> g1_rho;

  //!Elements [\hat{P}_i(chi)]_1 for i = 1, ..., n
  vector<G1<ppT>> g1_Pi_hats;

  //!Element [\hat{rho}]_1
  G1<ppT> g1_rho_hat;

  //!Element [P0(chi)]_1
  G1<ppT> g1_P0;

  //!Elements [((P_i(chi) + P_0(chi)^2 - 1) / rho)]_1 for i = 1,..., n
  vector<G1<ppT>> g1_Pi_longs;

  //!Element [sum P_i(chi)]_1 for i = 1,..., n
  G1<ppT> g1_sum_Pi;

  //!Element [sum \hat{P}_i(chi)]_1 for i = 1,..., n
  G1<ppT> g1_sum_Pi_hat;

  //!Elements [((beta * P_i(chi) - \hat{beta} * P_i^{hat}(chi))]_1 for i = 1,..., n
  vector<G1<ppT>> g1_Pi_longs2;

  //!Element [(beta * rho + \hat{beta} * \hat{rho})]_1
  G1<ppT> g1_beta_rho;



  //-----Group G2 elements of CRS-----

  //!Element [sk]_2
  G2<ppT> g2_sk;

  //!Elements [P_i(chi)]_2 for i = 1, ..., n
  vector<G2<ppT>> g2_Pis;

  //!Element [P0(chi)]_2
  G2<ppT> g2_P0;

  //!Element [rho]_2
  G2<ppT> g2_rho;

  //!Element [sum P_i(chi)]_2 for i = 1,..., n
  G2<ppT> g2_sum_Pi;

  //!Element [beta]_2
  G2<ppT> g2_beta;

  //!Element [\hat{beta}]_2
  G2<ppT> g2_beta_hat;



  /*! Constructs CRS
  * \param n number of ciphertexts
  */
  CRS(int n): n {n} {
    enter_block("CRS generation", true);
    ppT::init_public_params(); //initializes pairing parameters

    
    //Random generates secret values
    Fr<ppT> chi = Fr<ppT>::random_element();
    Fr<ppT> beta = Fr<ppT>::random_element();
    Fr<ppT> beta_hat = Fr<ppT>::random_element();
    Fr<ppT> rho = generate_nonzero();
    Fr<ppT> rho_hat = generate_nonzero();
    Fr<ppT> sk = Fr<ppT>::random_element();

    enter_block("Compute P_i(chi)-s", false);
    Fr<ppT> P0 = lagrangian(n + 1, chi) - 1;
    Fr_vector<ppT> Pis = generate_Pi(chi);
    leave_block("Compute P_i(chi)-s", false);


    //Computes Z(chi)
    Fr<ppT> Zchi = Fr<ppT>::one();
    for (int i = 1; i < n + 2; i++) {
      Zchi *= chi - i;
    }

    enter_block("Compute hat{P}_i(chi)-s", false);
    Fr_vector<ppT> Pi_hats;
    Fr<ppT> chi_n_1 = chi ^ (n + 1);
    Fr<ppT> temp = chi_n_1;
    for (int i = 0; i < n; i++) {
      temp = temp * chi_n_1;
      Pi_hats.push_back(temp);
    }
    leave_block("Compute hat{P}_i(chi)-s", false);

    enter_block("Init G1 elements", false);
    inhibit_profiling_info = true;
    init_G1_elems(chi, beta, beta_hat, rho, rho_hat, Pis, Pi_hats, P0);
    inhibit_profiling_info = false;
    leave_block("Init G1 elements", false);

    enter_block("Init G2 elements", false);
    inhibit_profiling_info = true;
    init_G2_elems(sk, beta, beta_hat, rho, Pis, P0);
    inhibit_profiling_info = false;
    leave_block("Init G2 elements", false);


    leave_block("CRS generation", true);
  }



private:
  /*! Generates CRS elements of group G1. See the paper for the meaning of parameters.
  * \param Pis polynomials Pi evaluated at point chi
  * \param P0 polynomial P0 evaluated at point chi
  */
  void init_G1_elems(Fr<ppT> chi, Fr<ppT> beta, Fr<ppT> beta_hat, Fr<ppT> rho, Fr<ppT> rho_hat,
                     Fr_vector<ppT> Pis, Fr_vector<ppT> Pi_hats, Fr<ppT> P0) {

    G1<ppT> g1 = G1<ppT>::one();

    //Precomputation for fixed-base multi-exponentiation of g1.
    const size_t g1_exp_count = 4 * n + 6;
    size_t g1_window_size = get_exp_window_size<G1<ppT>>(g1_exp_count);
    window_table<G1<ppT>> g1_table = get_window_table(Fr<ppT>::size_in_bits(),
                                     g1_window_size, g1);

    g1_Pis = batch_exp(Fr<ppT>::size_in_bits(), g1_window_size, g1_table, Pis);
    g1_rho = windowed_exp(Fr<ppT>::size_in_bits(), g1_window_size, g1_table, rho);

    g1_Pi_hats = batch_exp(Fr<ppT>::size_in_bits(), g1_window_size, g1_table, Pi_hats);
    g1_rho_hat = windowed_exp(Fr<ppT>::size_in_bits(), g1_window_size, g1_table, rho_hat);

    g1_P0 = windowed_exp(Fr<ppT>::size_in_bits(), g1_window_size, g1_table, P0);


    //field elements ((P_i(chi) + P0(chi))^2 - 1)/rho
    Fr_vector<ppT> Pi_longs;
    Fr<ppT> rho_inv = rho.inverse();
    for (int i = 0; i < n; i++) {
      Pi_longs.push_back((((Pis.at(i) + P0) ^ 2) - 1) * rho_inv);
    }

    g1_Pi_longs = batch_exp(Fr<ppT>::size_in_bits(), g1_window_size, g1_table, Pi_longs);

    Fr<ppT> sum_Pi = vector_sum<Fr<ppT>>(Pis);
    Fr<ppT> sum_Pi_hat = vector_sum<Fr<ppT>>(Pi_hats);

    g1_sum_Pi = windowed_exp(Fr<ppT>::size_in_bits(), g1_window_size, g1_table, sum_Pi);
    g1_sum_Pi_hat = windowed_exp(Fr<ppT>::size_in_bits(), g1_window_size, g1_table, sum_Pi_hat);


    //field elements ((beta * P_i(chi) + \hat{beta} * \hat{P}_i(chi))
    Fr_vector<ppT> Pi_longs2;
    for (int i = 0; i < n; i++) {
      Pi_longs2.push_back(beta * Pis.at(i) + beta_hat * Pi_hats.at(i));
    }

    g1_Pi_longs2 = batch_exp(Fr<ppT>::size_in_bits(), g1_window_size, g1_table, Pi_longs2);
    g1_beta_rho = windowed_exp(Fr<ppT>::size_in_bits(), g1_window_size, g1_table, beta * rho + beta_hat * rho_hat);
  }


  /*! Generates CRS elements of group G2. See the paper for the meaning of parameters.
  * \param sk ElGamal secret key
  */
  void init_G2_elems(Fr<ppT> sk, Fr<ppT> beta, Fr<ppT> beta_hat, Fr<ppT> rho, Fr_vector<ppT> Pis, Fr<ppT> P0) {
    G2<ppT> g2 = G2<ppT>::one();

    //Precomputation for fixed-based multi-exponentiation of g2.
    const size_t g2_exp_count = n + 6;
    size_t g2_window_size = get_exp_window_size<G2<ppT>>(g2_exp_count);
    window_table<G2<ppT>> g2_table = get_window_table(Fr<ppT>::size_in_bits(), g2_window_size, g2);

    g2_sk = windowed_exp(Fr<ppT>::size_in_bits(), g2_window_size, g2_table, sk);
    g2_Pis = batch_exp(Fr<ppT>::size_in_bits(), g2_window_size, g2_table, Pis);
    g2_rho = windowed_exp(Fr<ppT>::size_in_bits(), g2_window_size, g2_table, rho);
    g2_P0 = windowed_exp(Fr<ppT>::size_in_bits(), g2_window_size, g2_table, P0);

    Fr<ppT> sum_Pi = vector_sum<Fr<ppT>>(Pis);

    g2_sum_Pi = windowed_exp(Fr<ppT>::size_in_bits(), g2_window_size, g2_table, sum_Pi);
    g2_beta = windowed_exp(Fr<ppT>::size_in_bits(), g2_window_size, g2_table, beta);
    g2_beta_hat = windowed_exp(Fr<ppT>::size_in_bits(), g2_window_size, g2_table, beta_hat);
  }


  /*! Generates uniformly random nonzero field element.
  * \returns nonzero field element
  */
  Fr<ppT> generate_nonzero() {
    Fr<ppT> elem = Fr<ppT>::random_element();
    while (elem == Fr<ppT>::zero()) {
      elem = Fr<ppT>::random_element();
    }

    return elem;
  }


  /*! Evaluates Lagrange basis polynomial l_i at point x.
  * Distinct points are 1, 2, ..., n + 1.
  * \param i polynomial index in range 1, 2, ..., n + 1
  * \param chi input value of the polynomial
  * \returns nonzero field element
  */
  Fr<ppT> lagrangian(int i, Fr<ppT> chi) {
    Fr<ppT> numerator = Fr<ppT>::one();
    Fr<ppT> denominator = Fr<ppT>::one();
    for (int j = 1; j < n + 2; j++) {
      if (i == j) continue;

      numerator = numerator * (chi - j);
      Fr<ppT> elem = i - j;
      denominator = denominator * elem;
    }

    return numerator * denominator.invert();
  }


  /*! Computes denominators for Lagrange basis polynomials.
  * Uses distinct points 1, ...,k
  * \param k number of basis polynomials
  */
  Fr_vector<ppT> compute_denominators(int k) {

    Fr_vector<ppT> denominators;

    Fr<ppT> temp = Fr<ppT>::one();
    for (int i = 1; i <= k; i++) {
      if (i == 1) {
        for (int j = 2; j <= k; j++) {
          Fr<ppT> elem = i - j;
          temp = temp * elem;
        }

      } else if (i == k) {
        Fr<ppT> elem = Fr<ppT>::one() - k;
        temp = elem * temp;
      } else {

        Fr<ppT> inverse = i - 1 - k;
        inverse = inverse.invert();

        Fr<ppT> elem = i - 1;

        temp = elem * temp * inverse;
      }
      denominators.push_back(temp);
    }

    return denominators;
  }


  /*! Computes vector of elements P_i(chi) for i = 1, ..., n.
  * Uses Lagrange basis polynomials with distinct points 1, ..., n + 1
  * \param chi point of evaluation
  */
  Fr_vector<ppT> generate_Pi(Fr<ppT> chi) {
    Fr_vector<ppT> Pis;

    Fr<ppT> prod = Fr<ppT>::one();
    for (int j = 1; j < n + 2; j++) prod = prod * (chi - j);

    Fr_vector<ppT> denoms = compute_denominators(n + 1);

    Fr<ppT> missing_factor = chi - (n + 1);

    //l_{n + 1} (\chi)
    Fr<ppT> l_n_1 = prod * (missing_factor * denoms.at(n)).inverse();

    Fr<ppT> two = 2;
    for (int i = 1; i < n + 1; i++) {
      missing_factor = chi - i;
      Fr<ppT> l_i = prod * (missing_factor * denoms.at(i - 1)).inverse();
      Pis.push_back(two * l_i + l_n_1);
    }

    return Pis;
  }
};

#endif /* CRS_H_ */
