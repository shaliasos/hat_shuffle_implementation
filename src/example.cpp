#include <iostream>
#include <libff/algebra/curves/bn128/bn128_pp.hpp>

using namespace std;
using namespace libff;

/*-------PAIRING EXAMPLE---------
*Shows how to use pairing based on Barreto-Naehrig curves
*/

int main(int argc, char *argv[])
{
 bn128_pp::init_public_params(); //initializes pairing parameters

 //Fixes generators for groups G1, G2 and GT (G1/G2 are additive, GT is multiplicative)
 G1<bn128_pp> P1 = G1<bn128_pp>::one();
 G2<bn128_pp> P2 = G2<bn128_pp>::one();
 GT<bn128_pp> PT = bn128_pp::reduced_pairing(P1, P2);

 //Takes two random values for exponentiation
 Fr<bn128_pp> a = Fr<bn128_pp>::random_element();
 Fr<bn128_pp> b = Fr<bn128_pp>::random_element();

 //Computes the correspondic EC points
 G1<bn128_pp> Q1 = a * P1;
 G2<bn128_pp> Q2 = b * P2;
 cout << "Q1:\n";
 Q1.print();
 cout << "\nQ2:\n";
 Q2.print();

 //Pairing ...
 GT <bn128_pp> QT = bn128_pp::reduced_pairing(Q1, Q2);
 cout << "\nQT:\n";
 QT.print();

 cout << endl;
 //.. is equal to
 Fr<bn128_pp> c = a * b;
 cout << "\nPT ^ c:\n";
 (PT ^ c).print();

 return 0;
}

