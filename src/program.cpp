#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm> //has random_shuffle
#include <libff/algebra/curves/bn128/bn128_pp.hpp>
#include <libff/algebra/curves/public_params.hpp>
#include "crs.h"
#include "prover.h"
#include "verifier.h"
#include "shuffle_util.h"

using namespace std;

typedef bn128_pp curve; //Choose elliptic curve (currently Barreto-Naehrig)

/*!Generates a permutation vector containing numbers 0 to n - 1 in random order.
* Current implementation uses default c++ random generator.
* This is probably not suitable for crypto.
* \param n length of the permutation vector
* \return permutation vector
*/
vector<int> generate_perm(int n) {
  vector<int> permutation;
  for (int i = 0; i < n; i++) permutation.push_back(i);
  random_shuffle(permutation.begin(), permutation.end()); //uses default random generator!
  return permutation;
}

/*! Generates n ElGamal ciphertexts. Encrypted messages are random.
* \param n number of ciphertexts
* \param h ElGamal public key
* \returns ElGamal ciphertexts
*/
ElGamal_vector<curve> generate_ciphertexts(int n, G2<curve> h) {
  G2<curve> g2 = G2<curve>::one();
  Fr_vector<curve> randomizers = generate_randomizers<curve>(n, 256);
  ElGamal_vector<curve> input;
  for (int i = 0; i < n; i++) {
    Fr<curve> msg = Fr<curve>::random_element();
    ElGamal_pair<curve> ciphertext {randomizers.at(i) * g2, (msg * g2) + (randomizers.at(i) * h)};
    input.push_back(ciphertext);
  }

  //There is a faster version with mixed addition (can it be used?)

  return input;
}

int main(int argc, char *argv[])
{
  if (argc < 2)
    cerr << "Input parameter missing!" << '\n';

  istringstream ss(argv[1]);
  int n; //number of ciphertexts
  if (!(ss >> n))
    cerr << "Invalid number " << argv[1] << '\n';

  CRS<curve> crs {n};

  vector<int> permutation = generate_perm(n);
  Fr_vector<curve> randomizers_cipher = generate_randomizers<curve>(n, 256);
  Fr_vector<curve> randomizers_proof = generate_randomizers<curve>(n - 1, 256);
  Fr<curve> randomizer_t = Fr<curve>::random_element();

  ElGamal_vector<curve> ciphertexts = generate_ciphertexts(n, crs.g2_sk);

  Prover<curve> prover {crs};
  enter_block("Protocol", true);
  Proof<curve> proof = prover.prove(ciphertexts, permutation, randomizers_cipher, randomizers_proof, randomizer_t);

  Verifier<curve> verifier {crs, 40};
  cout << "Verification = " << verifier.verify(ciphertexts, proof) << endl;
  leave_block("Protocol", true);
  return 0;
}

