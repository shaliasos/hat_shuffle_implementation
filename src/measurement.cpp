#include <iostream>
#include <math.h>
#include <libff/algebra/curves/bn128/bn128_pp.hpp>
#include <libff/algebra/scalar_multiplication/multiexp.hpp>
#include <libff/common/profiling.hpp>
#include <lbff/algebra/evaluation_domain/evaluation_domain.hpp>
#include "shuffle_util.h"
#include "omp.h"
#include "time.h" //Crappy randomness for testing
#include "gmp.h"
#include <future>


using namespace std;
using namespace libff;

/*! This code is for research purpose only and is not directly related to shuffle implementation.
*   It measures efficiency of different operations in libff and some optimization ideas (that where not used in the end).
*   Input to the programm is the number of ciphertexts.
*/

typedef bn128_pp curve; //Choose elliptic curve (currently Barreto-Naehrig)


bool batch_test(G1_vector<bn128_pp> vec_lhs, G1_vector<bn128_pp> vec_rhs, G2<bn128_pp> const_lhs, G2<bn128_pp> const_rhs) {
  Fr_vector<bn128_pp> randomizers = generate_randomizers<Fr<bn128_pp>>(vec_lhs.size());
  const size_t chunks = omp_get_max_threads();

  G1<bn128_pp> prod_lhs = multi_exp<G1<bn128_pp>, Fr<bn128_pp>>(vec_lhs.begin(), vec_lhs.end(),
                          randomizers.begin(), randomizers.end(), chunks, true);

  G1<bn128_pp> prod_rhs = multi_exp<G1<bn128_pp>, Fr<bn128_pp>>(vec_rhs.begin(), vec_rhs.end(),
                          randomizers.begin(), randomizers.end(), chunks, true);

  GT<bn128_pp> lhs = bn128_pp::reduced_pairing(prod_lhs, const_lhs);
  GT<bn128_pp> rhs = bn128_pp::reduced_pairing(prod_rhs, const_rhs);

  return lhs == rhs;
}


bool atomic_bucket_test(int M, G1_vector<bn128_pp> vec_lhs, G1_vector<bn128_pp> vec_rhs,  G2<bn128_pp> const_lhs, G2<bn128_pp> const_rhs) {
  //construct buckets
  vector<vector<int>> buckets;
  for (int i = 0; i < M; i++) {
    buckets.push_back(vector<int>());
  }
  srand(time(NULL));

  for (unsigned i = 0; i < vec_lhs.size(); i++) {
    int bucket_index = rand() % M;
    (buckets.at(bucket_index)).push_back(i);
  }

  G1_vector<bn128_pp> products_lhs;
  G1_vector<bn128_pp> products_rhs;
  for (auto const& bucket : buckets) {
    G1<bn128_pp> product1 = G1<bn128_pp>::zero();
    G1<bn128_pp> product2 = G1<bn128_pp>::zero();
    for (auto const& pos : bucket) {
      product1 = product1 + vec_lhs.at(pos);
      product1 = product2 + vec_rhs.at(pos);
    }
  }

  return batch_test(products_lhs, products_rhs, const_lhs, const_rhs);
}


bool bucket_test(int m, int l, G1_vector<bn128_pp> vec_lhs, G1_vector<bn128_pp> vec_rhs,  G2<bn128_pp> const_lhs, G2<bn128_pp> const_rhs) {
  int M = 2 ^ m;
  int rounds = ceil(l / (m - 1));
  for (int i = 0; i < rounds; i++) {
    if (! atomic_bucket_test(M, vec_lhs, vec_rhs, const_lhs, const_rhs)) {
      return false;
    }
  }

  return true;
}


template<typename G>
vector<G> gen_rand(int n, int k) {
  Fr_vector<curve> field_vec = generate_randomizers<curve>(n, k);
  vector<G> vec;
  for (int i = 0; i < n; i++) vec.push_back(field_vec.at(i) * G::one());

  return vec;
}

template<typename G>
void product_test(int n, int k) {
  vector<G> vec = gen_rand<G>(n, k);
  Fr_vector<curve> field_vec = generate_randomizers<curve>(n, k);

  enter_block("product test", false);
  for (int i = 0; i < n; i++) field_vec.at(i) * vec.at(i);
  leave_block("product test", false);
}

void exponentiation_test(int n, int k) {
  Fr_vector<curve> field_vec = generate_randomizers<curve>(n, k);

  GT<curve> gt = GT<curve>::one();
  vector<GT<curve>> vec;
  for(int i = 0; i < n; i++) vec.push_back(gt ^ field_vec.at(i));

  Fr_vector<curve> exponents = generate_randomizers<curve>(n, k);

  enter_block("exponentiation in GT", false);
  for (int i = 0; i < n; i++) vec.at(i) ^ exponents.at(i);
  leave_block("exponentiation in GT", false);
}

template<typename G>
void multi_product_test(int n, int k, size_t chunks) {
  vector<G> vec = gen_rand<G>(n, k);
  Fr_vector<curve> field_vec = generate_randomizers<curve>(n, k);

  enter_block("multi-product test", false);
  multi_exp<G, Fr<curve>>(vec.begin(), vec.end(),
                          field_vec.begin(), field_vec.end(), chunks, true);
  leave_block("multi-product test", false);
}

template<typename G>
void fixed_base_product_test(int n, int k) {
  Fr_vector<curve> field_vec = generate_randomizers<curve>(n, k);

  enter_block("fixed base exp", false);

  size_t window_size = get_exp_window_size<G>(n);
  window_table<G> table = get_window_table(Fr<curve>::size_in_bits(),
                                        window_size, G::one());

  batch_exp(Fr<curve>::size_in_bits(), window_size, table, field_vec);

  leave_block("fixed base exp", false);
}

void pairing_product_test(int n, int k){
  G1_vector<curve> lhs = gen_rand<G1<curve>>(n, k);
  G2_vector<curve> rhs = gen_rand<G2<curve>>(n, k);

  enter_block("pairing product", false);
  GT<curve> prod = GT<curve>::one();
  inhibit_profiling_info = true;
  for(int i = 0; i < n; i++){
     prod = prod * curve::reduced_pairing(lhs.at(i), rhs.at(i));
  }
  inhibit_profiling_info = false;
  leave_block("pairing product", false);
}


void pairing_test(int n, int k){
  G1_vector<curve> lhs = gen_rand<G1<curve>>(n, k);
  G2_vector<curve> rhs = gen_rand<G2<curve>>(n, k);

  enter_block("pairing test", false);
  inhibit_profiling_info = true;
  for(int i = 0; i < n; i++) curve::reduced_pairing(lhs.at(i), rhs.at(i));

  inhibit_profiling_info = false;
  leave_block("pairing test", false);
}


void fast_pairing_product_test(int n, int k){
  G1_vector<curve> lhs = gen_rand<G1<curve>>(n, k);
  G2_vector<curve> rhs = gen_rand<G2<curve>>(n, k);

  enter_block("fast pairing product", false);

  Fqk<curve> result = Fqk<curve>::one();
  for (int i = 0; i < n; i++) result = result * curve::miller_loop(curve::precompute_G1(lhs.at(i)), curve::precompute_G2(rhs.at(i)));

  curve::final_exponentiation(result);
  leave_block("fast pairing product", false);
}


int main(int argc, char *argv[])
{

  curve::init_public_params();
  const size_t chunks = omp_get_max_threads(); //parallelized
  //const size_t chunks = 1;

  istringstream ss(argv[1]);
  int n; //number of ciphertexts
  if (!(ss >> n))
    cerr << "Invalid number " << argv[1] << '\n';
  cout << "n = " << n << endl;


  enter_block("generate 256 bit randomizers ", false);
  generate_randomizers<curve>(n, 256);
  leave_block("generate 256 bit randomizers ", false);


  enter_block("G1 tests", true);
  product_test<G1<curve>>(n, 40);
  multi_product_test<G1<curve>>(n, 256, chunks);
  fixed_base_product_test<G1<curve>>(n, 256);
  leave_block("G1 tests", true);


  enter_block("G2 tests", true);
  product_test<G2<curve>>(n, 256);
  multi_product_test<G2<curve>>(n, 256, chunks);
  fixed_base_product_test<G2<curve>>(n, 256);
  leave_block("G2 tests", true);

  exponentiation_test(n, 256);

  fast_pairing_product_test(n, 256);
  pairing_test(n, 256);

  /* Some other tests
  G1_vector<bn128_pp> vector_lhs = generate_randomizers<G1<bn128_pp>>(n);
  G1_vector<bn128_pp> vector_rhs = generate_randomizers<G1<bn128_pp>>(n);
  G2<bn128_pp> const_lhs = G2<bn128_pp>::random_element();
  G2<bn128_pp> const_rhs = G2<bn128_pp>::random_element();
  enter_block("bucket test", false);
  int m = 21;
  int l = 80;
  inhibit_profiling_info = true;
  bucket_test(m, l, vector_lhs, vector_rhs, const_lhs, const_rhs);
  inhibit_profiling_info = false;

  leave_block("bucket test", false);*/

  /*
  lhs = generate_randomizers<G1<bn128_pp>>(n);
  rhs = generate_randomizers<G2<bn128_pp>>(n);

  enter_block("double miller pairing product", false);
  inhibit_profiling_info = true;
  vector<G1_precomp<bn128_pp>> precomps3;
  for(int i = 0; i < n; i++) precomps3.push_back(bn128_pp::precompute_G1(lhs.at(i)));

  vector<G2_precomp<bn128_pp>> precomps4;
  for(int i = 0; i < n; i++) precomps4.push_back(bn128_pp::precompute_G2(rhs.at(i)));

  Fqk<bn128_pp> result2 = Fqk<bn128_pp>::one();
  for(int i = 0; i < n; i+=2){
      result2 = result2 * bn128_pp::double_miller_loop(precomps3.at(i), precomps4.at(i), precomps3.at(i + 1), precomps4.at(i + 1));
  }
  GT<bn128_pp> prod3 = bn128_pp::final_exponentiation(result2);

  inhibit_profiling_info = false;
  leave_block("double miller pairing product", false);
  */
  return 0;
}

